# Summary

## Введение

* [Introduction](/README.md)

## Элементы страниц

* [Списки](spiski.md)
  * [Deployments](deployments.md)
  * [Pods](pods.md)
  * [Volumes](volumes.md)
  * [Tokens](tokens.md)
  * [Services](services.md)
  * [Replica Sets](replica-sets.md)
* [Кнопки](knopki.md)
* [Всплывающие окна](vsplyvayushie-okna.md)
* [Описания](opisaniya.md)
* [Табы](taby.md)
* [Дополнительные элементы](dopolnitelnye-elementy.md)



